# MiniGame
####Created for [aviatorcraft.com](http://aviatorcraft.com) exclusively.


Written by Alex/Fireblazer & John/J_Boy1234


The **config.yml** contains;
* Current configuration stores points via **UUID**
* Points are displayed after the UUID.

The **plugin.yml** contains;
* The commands
* The permissions
* The main class path
* The authors
* The load time
* The website
