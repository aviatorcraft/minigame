package me.alex.aviatorcraft;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class RefreshScoreboard implements Listener {
	public static Object nestedObject;
	public main plugin;
    
    public RefreshScoreboard(Player current) {
		refreshScores(current);
	}

	@SuppressWarnings({ "deprecation", "static-access" })
	public void refreshScores(Player p) {
        plugin.getConfig().get("Points.Players." + p.getPlayer().getUniqueId(), 0);
        plugin.saveConfig();
        
        // seperate things
        
	    ScoreboardManager manager = Bukkit.getScoreboardManager();
	    Scoreboard sboard = manager.getNewScoreboard();
	    Team team = sboard.registerNewTeam("test-team");
	    
	    Objective objective = sboard.registerNewObjective("noyou", "sucks");
	    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
	    objective.setDisplayName(ChatColor.WHITE.BOLD + "AVIATORCRAFT");
	    Score line = objective.getScore(ChatColor.WHITE + "Deathmatch"); //Get a fake offline player
	    line.setScore(99);
	    
        List<Player> onlinePlayers = Arrays.asList(Bukkit.getServer().getOnlinePlayers());
        Iterator<Player> interator = onlinePlayers.iterator();
        int numjeff = 0;
        while(interator.hasNext()) {
        	Player onlinePlayer = interator.next();
        	numjeff++;
        	
    	    int points = plugin.getConfig().getInt("Points.Players." + onlinePlayer.getPlayer().getUniqueId());
    	    plugin.saveConfig();
    	    
    	    Score score = objective.getScore(ChatColor.GOLD + "" + points + " Points " + ChatColor.GRAY + onlinePlayer.getName());
        	
        	team.addPlayer(onlinePlayer);
        	score.setScore(numjeff);
        	onlinePlayer.setScoreboard(sboard);
        }
	}
	
	@EventHandler
    public void onDeath(PlayerDeathEvent e) {
    	e.setDeathMessage(null);
	    if (e.getEntityType() == EntityType.PLAYER) {
	    Player player = e.getEntity();
	    
	    if((player.getKiller() instanceof Player)) {
	    	refreshScores(player);
	    }
	}
}
}