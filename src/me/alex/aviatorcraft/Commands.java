package me.alex.aviatorcraft;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
 
public class Commands implements CommandExecutor{
        private InvGui gui;
 
        public Commands(InvGui gui){
                this.gui = gui;
        }
       
        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args){
               
                if(!(sender instanceof Player)){
                        return true;
                }
                if(args.length != 0){
                        return true;
                }
                Player p = (Player) sender;
               
                gui.openInventory(p);
               
        return true;
        }
}