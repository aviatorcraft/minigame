package me.alex.aviatorcraft;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
 
public class InvGui implements Listener{
 
@SuppressWarnings("unused")
private final main plugin;
 
public InvGui(main plugin) {
        this.plugin = plugin;
                       
}
Inventory inv;
 
@SuppressWarnings("static-access")
public void openInventory(Player p){
        inv = Bukkit.getServer().createInventory(null, 9, ChatColor.WHITE.BOLD + "Selection");
        p.openInventory(inv);
        inv.setItem(1, new ItemStack(Material.IRON_CHESTPLATE));
        inv.setItem(3, new ItemStack(Material.CHAINMAIL_CHESTPLATE));
        inv.setItem(5, new ItemStack(Material.GOLD_CHESTPLATE));
        inv.setItem(7, new ItemStack(Material.DIAMOND_CHESTPLATE));
}
 
@EventHandler
public void itemClick(InventoryClickEvent event){
        if(event.getInventory().getName().equalsIgnoreCase(ChatColor.BLUE + "Selection")){
                if(event.getCurrentItem() == null){
                        return;
                } }
                //event.setCancelled(true);
                //Jboycode Iron
                if(event.getCurrentItem().getType() == Material.IRON_CHESTPLATE){
                        Player p = (Player) event.getWhoClicked();
                        if (p.hasPermission("explosions.armour.iron")) {
                                event.getWhoClicked().closeInventory();
                                ItemStack[] iron = { new ItemStack(Material.IRON_HELMET), new ItemStack(Material.IRON_CHESTPLATE), new ItemStack(Material.IRON_LEGGINGS), new ItemStack(Material.IRON_BOOTS) };
                                event.getWhoClicked().getInventory().setArmorContents(iron);
                                p.sendMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "AviatorArmor" + ChatColor.BLACK + "] " + ChatColor.GRAY + "You put on IRON Armour!");
                                } else {
                                        p.sendMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "AviatorArmor" + ChatColor.BLACK + "] " + ChatColor.GRAY + "Sorry, but you do not have permission to put this on!");
                                }
                }
                //JboyCode gold
                if(event.getCurrentItem().getType() == Material.GOLD_CHESTPLATE){
                        Player p = (Player) event.getWhoClicked();
                        if (p.hasPermission("explosions.armour.gold")) {
                                event.getWhoClicked().closeInventory();
                                ItemStack[] gold = { new ItemStack(Material.GOLD_HELMET), new ItemStack(Material.GOLD_CHESTPLATE), new ItemStack(Material.GOLD_LEGGINGS), new ItemStack(Material.GOLD_BOOTS) };
                                event.getWhoClicked().getInventory().setArmorContents(gold);
                                p.sendMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "AviatorArmor" + ChatColor.BLACK + "] " + ChatColor.GRAY + "You put on GOLD Armour!");
                                } else {
                                        p.sendMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "AviatorArmor" + ChatColor.BLACK + "] " + ChatColor.GRAY + "Sorry, but you do not have permission to put this on!");
                                }
               
        }
                if(event.getCurrentItem().getType() == Material.DIAMOND_CHESTPLATE){
                        Player p = (Player) event.getWhoClicked();
                        if (p.hasPermission("explosions.armour.diamond")) {
                                event.getWhoClicked().closeInventory();
                                ItemStack[] diamond = { new ItemStack(Material.DIAMOND_HELMET), new ItemStack(Material.DIAMOND_CHESTPLATE), new ItemStack(Material.DIAMOND_LEGGINGS), new ItemStack(Material.DIAMOND_BOOTS) };
                                event.getWhoClicked().getInventory().setArmorContents(diamond);
                                p.sendMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "AviatorArmor" + ChatColor.BLACK + "] " + ChatColor.GRAY + "You put on DIAMOND Armour!");
                                } else {
                                        p.sendMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "AviatorArmor" + ChatColor.BLACK + "] " + ChatColor.GRAY + "Sorry, but you do not have permission to put this on!");
                                }
        }
 
        if(event.getCurrentItem().getType() == Material.CHAINMAIL_CHESTPLATE){
                Player p = (Player) event.getWhoClicked();
                if (p.hasPermission("explosions.armour.chain")) {
                        event.getWhoClicked().closeInventory();
                        ItemStack[] chain = { new ItemStack(Material.CHAINMAIL_HELMET), new ItemStack(Material.CHAINMAIL_CHESTPLATE), new ItemStack(Material.CHAINMAIL_LEGGINGS), new ItemStack(Material.CHAINMAIL_BOOTS) };
                        event.getWhoClicked().getInventory().setArmorContents(chain);
                        p.sendMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "AviatorArmor" + ChatColor.BLACK + "] " + ChatColor.GRAY + "You put on CHAINMAIL Armour!");
                        } else {
                                p.sendMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "AviatorArmor" + ChatColor.BLACK + "] " + ChatColor.GRAY + "Sorry, but you do not have permission to put this on!");
                        }
}
}
 
}