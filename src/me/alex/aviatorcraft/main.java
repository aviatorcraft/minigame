package me.alex.aviatorcraft;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


@SuppressWarnings("unused")
public class main extends JavaPlugin implements Listener {
	public main plugin;
	
	
	//Remove poison effects method, there probably a better way but this works
	public void clearPotionEffects(Player p)
	{
	if (p.hasPotionEffect(PotionEffectType.BLINDNESS) == true)
	{
	p.removePotionEffect(PotionEffectType.BLINDNESS);
	}
	if (p.hasPotionEffect(PotionEffectType.CONFUSION) == true)
	{
	p.removePotionEffect(PotionEffectType.CONFUSION);
	}
	if (p.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE) == true)
	{
	p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
	}
	if (p.hasPotionEffect(PotionEffectType.FAST_DIGGING) == true)
	{
	p.removePotionEffect(PotionEffectType.FAST_DIGGING);
	}
	if (p.hasPotionEffect(PotionEffectType.FIRE_RESISTANCE) == true)
	{
	p.removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
	}
	if (p.hasPotionEffect(PotionEffectType.HEAL) == true)
	{
	p.removePotionEffect(PotionEffectType.HEAL);
	}
	if (p.hasPotionEffect(PotionEffectType.HUNGER) == true)
	{
	p.removePotionEffect(PotionEffectType.HUNGER);
	}
	if (p.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE) == true)
	{
	p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
	}
	if (p.hasPotionEffect(PotionEffectType.JUMP) == true)
	{
	p.removePotionEffect(PotionEffectType.JUMP);
	}
	if (p.hasPotionEffect(PotionEffectType.POISON) == true)
	{
	p.removePotionEffect(PotionEffectType.POISON);
	}
	if (p.hasPotionEffect(PotionEffectType.REGENERATION) == true)
	{
	p.removePotionEffect(PotionEffectType.REGENERATION);
	}
	if (p.hasPotionEffect(PotionEffectType.SLOW) == true)
	{
	p.removePotionEffect(PotionEffectType.SLOW);
	}
	if(p.hasPotionEffect(PotionEffectType.SLOW_DIGGING) == true)
	{
	p.removePotionEffect(PotionEffectType.SLOW_DIGGING);
	}
	if (p.hasPotionEffect(PotionEffectType.SPEED) == true)
	    {
	        p.removePotionEffect(PotionEffectType.SPEED);
	    }
	if (p.hasPotionEffect(PotionEffectType.WATER_BREATHING) == true)
	{
	p.removePotionEffect(PotionEffectType.WATER_BREATHING);
	}
	if (p.hasPotionEffect(PotionEffectType.WEAKNESS) == true)
	{
	p.removePotionEffect(PotionEffectType.WEAKNESS);
	}
	 
	}
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		
		getServer().getPluginManager().registerEvents(new RefreshScoreboard((Player) this), this);
        getServer().getPluginManager().registerEvents(new InvGui(this), this);
        getCommand("inv").setExecutor(new Commands(new InvGui(this)));
        
        loadConfiguration();
	}
	@Override
	public void onDisable() {
		saveConfig();
	}
	
    public void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
    }
    
    // IN-GAME COMMANDS, YES.
    
    public ArrayList<UUID> ingame = new ArrayList<UUID>();
    public ArrayList<UUID> intime = new ArrayList<UUID>();
    public ArrayList arena = new ArrayList();
	public boolean onCommand(final CommandSender sender, Command cmd, String label, String[] args) {
		
		// - /points
		
		if(cmd.getName().equalsIgnoreCase("points")) {
			Player player = (Player) sender;
			Location location = player.getPlayer().getLocation();
			UUID playername = player.getPlayer().getUniqueId();
			int points = getConfig().getInt("Points.Players." + player.getUniqueId());
			
			sender.sendMessage(ChatColor.GREEN + "You have " + ChatColor.WHITE + points + " Total " + ChatColor.GOLD + "POINTS");
			Bukkit.getPlayer(playername).getWorld().playSound(location,Sound.NOTE_PIANO,10, 6);
		}
		
		// - /join & /gamejoin
		//Removed
		
		// - /leave & /gameleave
		
		if(cmd.getName().equalsIgnoreCase("gameleave") || cmd.getName().equalsIgnoreCase("leave")){
				Player p1 = (Player)sender;
				if((intime.contains(p1.getUniqueId()))){
					this.ingame.remove(p1.getUniqueId());
					this.intime.remove(p1.getUniqueId());
					clearPotionEffects(p1);
					getServer().dispatchCommand(Bukkit.getConsoleSender(), "ci " + sender.getName());
					getServer().dispatchCommand(p1, "spawn");
				    if(intime.contains(p1.getUniqueId())){
				    	Bukkit.broadcastMessage(ChatColor.WHITE + sender.getName() + " has " + ChatColor.RED + "left" + ChatColor.WHITE + " the game.");
				    }
				}
			}
			return true;
		}
		
	
@EventHandler
public void signPlace(SignChangeEvent e) {
    if(e.getLine(0).contains("[JoinGame]")) {
    	e.setLine(0, ChatColor.GREEN +"[JoinGame]");
    }
}
	//Main join for timed death match
@EventHandler
    public void signInteract(final PlayerInteractEvent e) {
	
	if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
		Block block = e.getClickedBlock();
	    if(block.getType() == Material.SIGN || block.getType() == Material.SIGN_POST || block.getType() == Material.WALL_SIGN) {
	    Sign sign = (Sign) e.getClickedBlock().getState();
		    if(sign.getLine(2).contains("[DMTIMED]")) {
			    if(!(intime.contains(e.getPlayer().getUniqueId()))){
				    this.intime.add(e.getPlayer().getUniqueId());
				    Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				    	Player p = (Player)e.getPlayer();
						
				    	public void run() {	
					    	Player p = (Player)e.getPlayer();
							p.getInventory().clear();
							p.getInventory().addItem(new ItemStack(Material.ARROW, 1));
							p.getInventory().addItem(new ItemStack(Material.FIREBALL, 1));
							p.getInventory().addItem(new ItemStack(Material.TNT, 24));
					    	p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 500000, 2));
					    	p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500000, 2));
					    	p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 500000, 4));
					    	p.setSaturation(10000);
					    	
					    }
				    },1 );
				    
				    
				    if(intime.contains(e.getPlayer().getUniqueId())){
				    	Bukkit.broadcastMessage(ChatColor.WHITE + e.getPlayer().getName() + " has " + ChatColor.GREEN + "joined" + ChatColor.WHITE + " the minigame.");
				    }
				    
				    Player p = (Player)e.getPlayer();
				     
				    this.arena.add(e.getPlayer().getName() + " " + sign.getLine(1));
				    
				   	Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "warp " + sign.getLine(1) + " " + e.getPlayer().getName());
				    
				   
				    
			    }else{
			    	e.getPlayer().sendMessage("AviatorCraft Games - You are aleady in a game! If this is not the case, Please ReLog.");
			    }
			    	
			    }
			    	
		    	
		    }
		    	
		    
		    
		    
		    

		    	
		    	
		    	
		    	
		    	
		    	
		    	
		    	
		
	}
	
	
	
	
	
	
	
	
	
	
	
	    if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
	    Block block = e.getClickedBlock();
		    if(block.getType() == Material.SIGN || block.getType() == Material.SIGN_POST || block.getType() == Material.WALL_SIGN) {
		    Sign sign = (Sign) e.getClickedBlock().getState();
			    if(sign.getLine(0).contains("[JoinGame]")) {
	    	if(!(ingame.contains(e.getPlayer().getUniqueId()))){
	
		    Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
		    	Player p = (Player)e.getPlayer();
				
		    	public void run() {	
			    	Player p = (Player)e.getPlayer();
					p.getInventory().clear();
					p.getInventory().addItem(new ItemStack(Material.ARROW, 1));
					p.getInventory().addItem(new ItemStack(Material.FIREBALL, 1));
					p.getInventory().addItem(new ItemStack(Material.TNT, 24));
			    	p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 500000, 2));
			    	p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500000, 2));
			    	p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 500000, 4));
			    	p.setSaturation(10000);
			    	Bukkit.broadcastMessage(ChatColor.WHITE + p.getName() + " has " + ChatColor.GREEN + "joined" + ChatColor.WHITE + " the minigame.");
			    }
		    },1 );
		    
	    Player p = (Player)e.getPlayer();
	    this.ingame.add(p.getUniqueId());
	    e.getPlayer().sendMessage("Now joining this game"); // TODO: ADD PREFIX AND ARENA VAR
	   	Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "warp " + sign.getLine(1) + " " + e.getPlayer().getName());
			    }
		    }
	    }
	}
}

@EventHandler
public void onPlayerQuit(PlayerQuitEvent e){
	if((intime.contains(e.getPlayer().getUniqueId()))){
		this.intime.remove(e.getPlayer().getUniqueId());
		 e.getPlayer().getServer().dispatchCommand(e.getPlayer(), "gameleave");
		 clearPotionEffects(e.getPlayer());
	}
}

@EventHandler
public boolean onPlayerInteract0(PlayerInteractEvent eventy) {
	if(this.intime.contains(eventy.getPlayer().getUniqueId())){
	if (eventy.getPlayer().getItemInHand().getType() == Material.ARROW) {
		if(eventy.getAction().equals(Action.LEFT_CLICK_AIR)) {
			eventy.getPlayer().launchProjectile(Arrow.class);
//		        Random rand = new Random();
//		        int randomNumber = rand.nextInt(8);
//		        
//		        eventy.getPlayer().getWorld().playSound(eventy.getPlayer().getLocation(),Sound.FIZZ,10, randomNumber);
		}
	}
	}
	return true;	
}
	
@EventHandler
public boolean onPlayerDeath(final PlayerDeathEvent e) {
	if(this.intime.contains(e.getEntity().getUniqueId())){
	if(e.getEntity() instanceof Player) {
		e.setDroppedExp(0);
		e.getDrops().clear();
		final Location loc = e.getEntity().getLocation();
		ParticleEffect.SMOKE_NORMAL.display(4, 15,0,0,0, loc.add(0,2,0), 100);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
            public void run() {
                    Firework f = (Firework) loc.getWorld().spawn(loc, Firework.class);
                   
                    FireworkMeta fm = f.getFireworkMeta();
                    fm.addEffect(FireworkEffect.builder()
                                    .flicker(false)
                                    .trail(true)
                                    .with(Type.BURST)
                                    .withColor(Color.WHITE)
                                    .withFade(Color.RED)
                                    .build());
                    fm.setPower(3);
                    f.setFireworkMeta(fm);
                    e.getEntity().getServer().dispatchCommand(e.getEntity(), "gameleave"); //SENDS LEAVE CMD
            }
		}, 20);
	}
	}
	return true;
}
	
@EventHandler
public void onPlayerInteract1(PlayerInteractEvent eventy) {
	if(this.intime.contains(eventy.getPlayer().getUniqueId())){
	if (eventy.getPlayer().getItemInHand().getType() == Material.TNT) {
	if(eventy.getAction().equals(Action.LEFT_CLICK_AIR) || eventy.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
		final Vector direction = eventy.getPlayer().getEyeLocation().getDirection().multiply(1);
		Location location  = eventy.getPlayer().getLocation();
	    TNTPrimed tnt = (TNTPrimed) location.getWorld().spawn(eventy.getPlayer().getEyeLocation().add(direction.getX(), direction.getY(), direction.getZ()), TNTPrimed.class);
	    tnt.setVelocity(direction);
	    Player player = eventy.getPlayer();
	    
        Random rand = new Random();
        int randomNumber = rand.nextInt(8);
        
        player.getWorld().playSound(location,Sound.FIREWORK_LAUNCH,10, randomNumber);
	    
	    if(player.getInventory().getItemInHand().getAmount() <= 1) {
	    	player.getInventory().setItemInHand(null); 
	    } else {
	    	player.getInventory().getItemInHand().setAmount(player.getInventory().getItemInHand().getAmount()-1);
	    }
	    
	}
	}
	}
}

@EventHandler
public boolean onPlayerInteract2(PlayerInteractEvent eventy) {
	if(this.intime.contains(eventy.getPlayer().getUniqueId())){
	if (eventy.getPlayer().getItemInHand().getType() == Material.FIREBALL) {
		if(eventy.getAction().equals(Action.LEFT_CLICK_AIR)) {
			eventy.getPlayer().launchProjectile(SmallFireball.class);
	        Random rand = new Random();
	        int randomNumber = rand.nextInt(8);
	        
	        eventy.getPlayer().getWorld().playSound(eventy.getPlayer().getLocation(),Sound.FIRE_IGNITE,10, randomNumber);
		}
	}
	}
	return true;	
}

@EventHandler
public void onBlockPlace(BlockPlaceEvent event) {
	if(this.intime.contains(event.getPlayer().getUniqueId())){
    if(event.getBlock().getType().equals(Material.TNT)) { // if they try to place TNT, don't let them
    	event.setCancelled(true);
    }        
	}
}

	
@EventHandler(priority = EventPriority.MONITOR)
public void onEntityDeath(final EntityDeathEvent e) {
	if(intime.contains(e.getEntity().getUniqueId())){
	 Bukkit.broadcastMessage("A player has died");
	}
    
}
	
@EventHandler
public void onDeath(PlayerDeathEvent e) {
	e.setDeathMessage(null);

	if(this.intime.contains(e.getEntity().getUniqueId())){
	    if (e.getEntityType() == EntityType.PLAYER) {
		    Player player = e.getEntity();
		    if((player.getKiller() instanceof Player)) {
		    	int current = getConfig().getInt("Points.Players." + player.getKiller().getUniqueId());
		    	
		    	
		    	getConfig().set("Points.Players." + player.getKiller().getUniqueId(), current + 1);
		    	saveConfig();
		    	//has to be after
		    	int killerpoints = getConfig().getInt("Points.Players." + player.getKiller().getUniqueId());
		    	
		    	player.getKiller().sendMessage(ChatColor.WHITE+"+1 " + ChatColor.GREEN + "Point, " + ChatColor.WHITE + killerpoints + " total.");
		    	Bukkit.broadcastMessage(ChatColor.RED + player.getName() + ChatColor.GREEN + " was killed by " + ChatColor.WHITE + player.getKiller().getName());
		    	
		    	RefreshScoreboard Player = new RefreshScoreboard(player);
		    	
		    }
	    }
	}
}




//Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "warp " + "arena" + " " +




@EventHandler
public void onPlayerDeath11(PlayerDeathEvent e)
{
	if(this.intime.contains(e.getEntity().getUniqueId())){
  final Player player = e.getEntity();
	this.intime.remove(e.getEntity().getUniqueId());
  Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
    public void run()
    {
      if (player.isDead()) {
        try
        {
          Object nmsPlayer = player.getClass().getMethod("getHandle", new Class[0]).invoke(player, new Object[0]);
          Object packet = Class.forName(nmsPlayer.getClass().getPackage().getName() + ".PacketPlayInClientCommand").newInstance();
          Class<?> enumClass = Class.forName(nmsPlayer.getClass().getPackage().getName() + ".EnumClientCommand");
          for (Object ob : enumClass.getEnumConstants()) {
            if (ob.toString().equals("PERFORM_RESPAWN")) {
              packet = packet.getClass().getConstructor(new Class[] { enumClass }).newInstance(new Object[] { ob });
            }
          }
          Object con = nmsPlayer.getClass().getField("playerConnection").get(nmsPlayer);
          con.getClass().getMethod("a", new Class[] { packet.getClass() }).invoke(con, new Object[] { packet });
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    }
  });
}
}


}

